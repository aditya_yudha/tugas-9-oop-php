<?php
require_once('animal.php');

$animal = new Animal('cat', 4, 'no');
$animalData = $animal->getAnimalData();

echo "Name: " . $animalData['name'] . "<br>";
echo "Legs: " . $animalData['legs'] . "<br>";
echo "Cold-blooded: " . $animalData['coolBlooded'] . "<br>";

$sungokong = new Ape('kera sakti',2,'no');
$sungokong->yell();
echo "<br>";
$frog = new Frog('kodok',4,'yes');
$frog->jump();
?>