<?php

class Animal {
  public $name;
  public $legs;
  public $coolBlooded;

  function __construct($name, $legs, $coolBlooded) {
    $this->name = $name;
    $this->legs = $legs;
    $this->coolBlooded = $coolBlooded;
  }
  function getAnimalData() {
    return [
      'name' => $this->name,
      'legs' => $this->legs,
      'coolBlooded' => $this->coolBlooded,
    ];
  }
}


class Frog extends Animal{
    function jump(){
        echo "hop hop";
    }
}

class Ape extends Animal{
    public $legs = 2;
    function yell(){
        echo "Auooo";
    }
}
?>